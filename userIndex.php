<?php

require_once 'vendor/autoload.php';

require_once 'userapi.php';

require_once 'init.php';

require_once 'utils.php';

require_once 'index.php';

// Define app routes
// main profile overview
$app->get('/user', function ($request, $response, $args) {
    if (isset($_SESSION['currentUser'])) {
        return $this->view->render($response, 'user/profileIndex.html.twig', ['currentUser' => $_SESSION['currentUser']]);
    } else {
        return $this->view->render($response, 'index.html.twig');
    }
});

// go to posts overview
$app->get('/user/posts/{userId:[0-9]+}', function ($request, $response, $args) {
    $userPosts = DB::query("SELECT * FROM posts WHERE userId=%d", $args['userId']);
    return $this->view->render($response, 'user/profilePosts.html.twig', ['userPosts' => $userPosts]);
});

// go to comments overview
$app->get('/user/comments/{userId:[0-9]+}', function ($request, $response, $args) {
    //$userComments = DB::query("SELECT * FROM comments WHERE userId=%d", $args['userId']);
    $userComments = DB::query(
        "SELECT 
        c.content, c.likes, c.createdAt
        FROM 
        comments as c
        INNER JOIN 
        posts as p 
        ON 
        c.postId = p.postId
        AND
        c.userId=%d",
        $args['userId']
    );
    return $this->view->render($response, 'user/profileComments.html.twig', ['userComments' => $userComments]);
});

// go to user profile
$app->get('/user/manage/{userId:[0-9]+}', function ($request, $response, $args) {
    $selectedUser = DB::queryFirstRow("SELECT * FROM users WHERE userId=%d", $args['userId']);
    return $this->view->render($response, 'user/profileManage.html.twig', ['selectedUser' => $selectedUser]);
});

// Update user profile
$app->post('/user/manage/{userId:[0-9]+}', function ($request, $response, $args) use ($log) {
    $selectedUser = DB::queryFirstRow("SELECT * FROM users WHERE userId=%d", $args['userId']);
    $location = $_SERVER['REQUEST_URI'];
    $FN = $request->getParam('FN');
    $MN = $request->getParam('MN');
    $LN = $request->getParam('LN');
    $date = $request->getParam('dob');
    $emailcurrent = $selectedUser['emailAddress'];
    $emailnew = $request->getParam('email');
    $city = $request->getParam('city');
    $password = $request->getParam('pass');
    $edited = Date('Y-m-d H:i:s');

    $errorList = [];
    if (preg_match('/^[a-zA-Z0-9 \,\'\.\-]{2,100}$/', $FN) !== 1) {
        $errorList[] = "First name must be 2-100 characters long, accepted characters are uppercase/lowercase letters, spaces, commas, hypens and apostrophes.";
    }
    if ($MN != null) {
        if (preg_match('/^[a-zA-Z0-9 \,\'\.\-]{2,100}$/', $MN) !== 1) {
            $errorList[] = "Middle name must be 2-100 characters long, accepted characters are uppercase/lowercase letters, spaces, commas, hypens and apostrophes.";
        }
    }
    if (preg_match('/^[a-zA-Z0-9 \,\'\.\-]{2,100}$/', $LN) !== 1) {
        $errorList[] = "Last name must be 2-100 characters long, accepted characters are uppercase/lowercase letters, spaces, commas, hypens and apostrophes.";
    }
    if (filter_var($emailnew, FILTER_VALIDATE_EMAIL) === FALSE) {
        $errorList[] = "Email does not look valid";
    } else {
        $result = DB::queryFirstRow("SELECT * FROM users WHERE emailAddress='$emailnew'");
        if (!$result) {
            echo "SQL Query failed";
            exit;
        }
        if ($result && $emailnew != $emailcurrent) {
            $errorList[] = "This email is already registered!";
        }
    }
    if (validateDate($date)) {
        $DoB = DateTime::createFromFormat('d/m/Y', $date);
    } else {
        $errorList[] = "Please enter a valid date in the format dd/mm/YYYY";
    }
    if ($city != null) {
        if (preg_match('/^[a-zA-Z0-9 \,\'\.\-]{2,100}$/', $city) !== 1) {
            $errorList[] = "City name must be 1-150 characters long, accepted characters are uppercase/lowercase letters, spaces, commas, hypens and apostrophes.";
        }
    }
    // verify image
    $hasPhoto = false;
    $mimeType = "";
    $uploadedImage = $request->getUploadedFiles()['image'];
    if ($uploadedImage->getError() != UPLOAD_ERR_NO_FILE) { // was anything uploaded?
        // print_r($uploadedImage->getError());
        $hasPhoto = true;
        $result = verifyUploadedPhoto($uploadedImage, $mimeType);
        if ($result !== TRUE) {
            $errorList[] = $result;
        }
    }
    if ($errorList) { // STATE 2: errors
        setFlashMessage("Errors");
        return $this->view->render($response, 'user/profileManage.html.twig', ['selectedUser' => $selectedUser, 'errorList' => $errorList]);
    } else { // STATE 3: success
        $photoData = null;
        if ($hasPhoto) {
            $photoData = file_get_contents($uploadedImage->file);
            $directory = $this->get('upload_directory');
            $uploadedImagePath = moveUploadedFile($directory, $uploadedImage);
        }
        // overwrite with new picture
        if ($uploadedImagePath) {
            // this stopped working ERROR main.ERROR: Database error: Badly formatted SQL query: Expected array, got scalar instead!
            // $valuesList = [
            //     'firstName' => $FN,
            //     'lastName' => $LN,
            //     'middleName' => $MN,
            //     'emailAddress' => $emailnew,
            //     'dateofBirth' => $DoB,
            //     'currentCity' => $city,
            //     'profilePicturePath' => $uploadedImagePath,
            //     'createdAt' => $created,
            //     'updatedAt' => $edited
            // ];

            // either update password, or leave
            if ($password != "") { // this is super messy, but we couldn't figure out the error
                DB::update('users', [
                    'firstName' => $FN,
                    'lastName' => $LN,
                    'middleName' => $MN,
                    'emailAddress' => $emailnew,
                    'dateofBirth' => $DoB,
                    'currentCity' => $city,
                    'profilePicturePath' => $uploadedImagePath,
                    'password' => $password,
                    'updatedAt' => $edited
                ], "userId=%i", $args['userId']);
            } else {
                DB::update('users', [
                    'firstName' => $FN,
                    'lastName' => $LN,
                    'middleName' => $MN,
                    'emailAddress' => $emailnew,
                    'dateofBirth' => $DoB,
                    'currentCity' => $city,
                    'profilePicturePath' => $uploadedImagePath,
                    'updatedAt' => $edited
                ], "userId=%i", $args['userId']);
            }
        }
        // keep old picture
        else if ($selectedUser['profilePicturePath'] && $uploadedImagePath == null) {
            // $valuesList = [
            //     'firstName' => $FN,
            //     'lastName' => $LN,
            //     'middleName' => $MN,
            //     'emailAddress' => $emailnew,
            //     'dateofBirth' => $DoB,
            //     'currentCity' => $city,
            //     'profilePicturePath' => $selectedUser['profilePicturePath'],
            //     'updatedAt' => $edited
            // ];
            // either update password, or leave
            if ($password != "") { // this is super messy, but we couldn't figure out the error
                DB::update('users', [
                    'firstName' => $FN,
                    'lastName' => $LN,
                    'middleName' => $MN,
                    'emailAddress' => $emailnew,
                    'dateofBirth' => $DoB,
                    'currentCity' => $city,
                    'profilePicturePath' => $selectedUser['profilePicturePath'],
                    'password' => $password,
                    'updatedAt' => $edited
                ], "userId=%i", $args['userId']);
            } else {
                DB::update('users', [
                    'firstName' => $FN,
                    'lastName' => $LN,
                    'middleName' => $MN,
                    'emailAddress' => $emailnew,
                    'dateofBirth' => $DoB,
                    'currentCity' => $city,
                    'profilePicturePath' => $selectedUser['profilePicturePath'],
                    'updatedAt' => $edited
                ], "userId=%i", $args['userId']);
            }
        } else { // no picture on account, no picture to upload
            // either update password, or leave
            if ($password != "") { // this is super messy, but we couldn't figure out the error
                DB::update('users', [
                    'firstName' => $FN,
                    'lastName' => $LN,
                    'middleName' => $MN,
                    'emailAddress' => $emailnew,
                    'dateofBirth' => $DoB,
                    'currentCity' => $city,
                    'password' => $password,
                    'updatedAt' => $edited
                ], "userId=%i", $args['userId']);
            } else {
                DB::update('users', [
                    'firstName' => $FN,
                    'lastName' => $LN,
                    'middleName' => $MN,
                    'emailAddress' => $emailnew,
                    'dateofBirth' => $DoB,
                    'currentCity' => $city,
                    'updatedAt' => $edited
                ], "userId=%i", $args['userId']);
            }
        }
        // DB::update('users', $valuesList, "userId=%i", $args['userId']);
        // $log->debug("User with userId=%s updated from IP=%s", $args['userId'], $_SERVER['REMOTE_ADDR']);
        setFlashMessage("User Account Successfully Updated!");
        return $response->withStatus(302)->withHeader(
            'Location',
            $location
        );
    }
});
