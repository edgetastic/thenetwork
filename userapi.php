<?php

require_once 'init.php';

require_once 'userIndex.php';

require_once 'utils.php';

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Http\UploadedFile;

// Define app routes
$app->get('/', function (Request $request, Response $response, array $args) {
    if (!isset($_SESSION['currentUser'])) {
        return $this->view->render($response, 'index.html.twig');
    } else {
        if ($_SESSION['currentUser']['type'] === "admin") {
            return $this->view->render($response, 'admin/adminIndex.html.twig');
        }
        return $this->view->render($response, 'logged_user.html.twig');
    }
});


$app->post('/register', function (Request $request, Response $response, array $args) use ($log) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $json = $request->getBody();
    $item = json_decode($json, TRUE);

    if (($result = validateRegistration($item)) !== TRUE) {
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - " . $result));
        return $response;
    }

    $dateOfBirth = $item['dateOfBirth'];
    $dateOfBirth = str_replace('/', '-', $dateOfBirth);
    $dateOfBirth = date('Y-m-d', strtotime($dateOfBirth));

    $item['dateOfBirth'] = $dateOfBirth;
    $passwordHash = password_hash($item['password'], PASSWORD_BCRYPT);
    $item['password'] = $passwordHash;
    $item['profilePicturePath'] = "default.jpg";

    DB::insert('users', $item);
    $insertId = DB::insertId();
    $log->debug("Record user added id=" . $insertId);
    $response = $response->withStatus(201);
    $response->getBody()->write(json_encode($insertId));
    $_SESSION['currentUser'] = $item;
    $_SESSION['currentUser']['userId'] = $insertId;
    return $response;
});

$app->post('/login', function (Request $request, Response $response, array $args) use ($log) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $json = $request->getBody();
    $item = json_decode($json, TRUE);

    $providedEmail = $item['emailAddress'];
    $providedPassword = $item['password'];

    $result = DB::queryFirstRow("SELECT * FROM users where emailAddress=%s", $providedEmail);
    if (!$result) {
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - invalid email"));
        return $response;
    }

    $password = $result['password'];

    if (!password_verify($providedPassword, $password)) {
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - Invalid password"));
        return $response;
    }

    $_SESSION['currentUser'] = $result;
    unset($_SESSION['currentUser']['password']);
    $log->debug("User logged in: " . $_SESSION['currentUser']['emailAddress']);
    $response = $response->withStatus(201);
    $response->getBody()->write(json_encode($_SESSION['currentUser']['userId']));
    return $response;
});

$app->get('/logout', function (Request $request, Response $response, array $args) use ($log) {
    if (isset($_SESSION['currentUser'])) {
        unset($_SESSION['currentUser']);
    }
    $response = $response->withStatus(201);
    $response->getBody()->write(json_encode("Logout successful"));
    //return $this->view->render($response, 'base.html.twig');
    return $response;
});

$app->get('/notifications', function (Request $request, Response $response, array $args) use ($log) {
    $notificationList = DB::query("SELECT n.*, u.firstName, u.middleName, u.lastName, u.profilePicturePath FROM notifications as n
                                INNER JOIN users as u on n.notifierId = u.userId
                                WHERE (n.userId=%i) AND (n.isRead = 0)
                                ORDER BY n.createdAt DESC", $_SESSION['currentUser']['userId']);


    // foreach($notificationList as &$notification) {
    //     DB::update('notifications', ['isRead' => 1], "notificationId=%i", $notification['notificationId']);
    // }

    return $this->view->render($response, 'notifications.html.twig', ['notificationList' => $notificationList]);
});

$app->get('/feed', function (Request $request, Response $response, array $args) use ($log) {
    $postList = DB::query("SELECT u.firstName, u.middleName, u.lastName, u.profilePicturePath, p.* FROM users AS u
                        INNER JOIN contacts as c on c.contactUserId = u.userId
                        INNER JOIN posts as p on p.userId = u.userId
                        WHERE c.userId = %i
                        ORDER BY p.createdAt desc", $_SESSION['currentUser']['userId']);
    foreach ($postList as &$post) {
        $dateTime = strtotime($post['createdAt']);
        $postedDate = date('Y-m-d', $dateTime);
        $post['createdAt'] = $postedDate;

        $commentList = DB::query("SELECT c.*, u.userId, u.firstName, u.middleName, u.lastName, u.profilePicturePath FROM comments as c
                                    INNER JOIN posts as p on p.postId = c.postId
                                    INNER JOIN users as u on u.userId = c.userId
                                    WHERE c.postId = %d
                                    ORDER BY c.createdAt desc", $post['postId']);

        foreach ($commentList as &$comment) {
            $dateTime = strtotime($comment['createdAt']);
            $postedDate = date('Y-m-d', $dateTime);
            $comment['createdAt'] = $postedDate;
        }
        $post['comments'] = $commentList;
    }
    return $this->view->render($response, 'user_feed.html.twig', ['postList' => $postList]);
});

$app->get('/feedProfile', function (Request $request, Response $response, array $args) use ($log) {
    $postList = DB::query("SELECT u.firstName, u.middleName, u.lastName, u.profilePicturePath, p.* 
                        FROM users as u 
                        INNER JOIN posts as p 
                        on p.userId = u.userId 
                        WHERE u.userId = %i
                        ORDER BY p.createdAt desc", $_SESSION['currentUser']['userId']);
    foreach ($postList as &$post) {
        $dateTime = strtotime($post['createdAt']);
        $postedDate = date('Y-m-d', $dateTime);
        $post['createdAt'] = $postedDate;

        $commentList = DB::query("SELECT c.*, u.userId, u.firstName, u.middleName, u.lastName, u.profilePicturePath FROM comments as c
                                    INNER JOIN posts as p on p.postId = c.postId
                                    INNER JOIN users as u on u.userId = c.userId
                                    WHERE c.postId = %d
                                    ORDER BY c.createdAt desc", $post['postId']);

        foreach ($commentList as &$comment) {
            $dateTime = strtotime($comment['createdAt']);
            $postedDate = date('Y-m-d', $dateTime);
            $comment['createdAt'] = $postedDate;
        }
        $post['comments'] = $commentList;
    }
    return $this->view->render($response, 'profile_feed.html.twig', ['postList' => $postList]);
});

$app->patch('/post/{postId:[0-9]+}/like', function (Request $request, Response $response, array $args) use ($log) {
    $postId = $args['postId'];
    $post = DB::queryFirstRow("SELECT * FROM posts WHERE postId=%d", $postId);
    $postLikes = $post['likes'];
    $postLikes++;
    DB::update('posts', ['likes' => $postLikes], "postId=%d", $postId);
    // Create a notification
    $notificationObj = [
        'userId' => $post['userId'],
        'type' => 'like',
        'notifierId' => $_SESSION['currentUser']['userId'],
        'postId' => $postId
    ];

    DB::insert('notifications', $notificationObj);

    $response = $response->withStatus(203);
    $response->getBody()->write(json_encode($postId));
    return $response;
});

$app->post('/post/comment', function (Request $request, Response $response, array $args) use ($log) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $json = $request->getBody();
    $item = json_decode($json, TRUE);
    $item['userId'] = $_SESSION['currentUser']['userId'];

    DB::insert('comments', $item);

    // Create notification on comment
    $post = DB::queryFirstRow("SELECT * FROM posts WHERE postId=%i", $item['postId']);
    $notificationObj = ['userId' => $post['userId'], 'type' => 'comment', 'notifierId' => $item['userId'], 'postId' => $item['postId']];
    DB::insert('notifications', $notificationObj);

    $insertId = DB::insertId();
    $log->debug("Record comment added id=" . $insertId);
    $response = $response->withStatus(201);
    $response->getBody()->write(json_encode($insertId));
    return $response;
});

$app->post('/submitPost', function (Request $request, Response $response, array $args) use ($log) {
    $data = $request->getParsedBody();
    $data['userId'] = $_SESSION['currentUser']['userId'];

    $hasPhoto = false;
    $mimeType = "";
    $uploadPicture = $request->getUploadedFiles()['file'];

    if ($uploadPicture->getError() !== UPLOAD_ERR_NO_FILE) {
        $hasPhoto = true;
        $result = verifyUploadedPhoto($uploadPicture, $mimeType);
        if ($result !== TRUE) {
            $response = $response->withStatus(400);
            $response->getBody()->write(json_encode("400 - " . $result));
            return $response;
        }

        $photoData = null;
        if ($hasPhoto) {
            $photoData = file_get_contents($uploadPicture->file);
            $directory = $this->get('upload_directory');
            $uploadedImagePath = moveUploadedFile($directory, $uploadPicture);
            $data['picturePath'] = $uploadedImagePath;
        }
    }

    DB::insert('posts', $data);
    $insertId = DB::insertId();
    $log->debug("Record posts added id=" . $insertId);
    $response = $response->withStatus(201);
    $response->getBody()->write(json_encode($insertId));
    return $response;
});

$app->post('/post/{postId:[0-9]+}/report', function (Request $request, Response $response, array $args) use ($log) {
    $postId = $args['postId'];
    $plaintiffId = $_SESSION['currentUser']['userId'];
    $reportObj = [
        'plaintiffId' => $plaintiffId,
        'postId' => $postId,
        'type' => 'post'
    ];

    DB::insert('reports', $reportObj);
    $insertId = DB::insertId();
    $log->debug("Record report added id=" . $insertId);
    $response = $response->withStatus(201);
    $response->getBody()->write(json_encode($insertId));
    return $response;
});

$app->post('/comment/{commentId:[0-9]+}/report', function (Request $request, Response $response, array $args) use ($log) {
    $commentId = $args['commentId'];
    $plaintiffId = $_SESSION['currentUser']['userId'];
    $reportObj = [
        'plaintiffId' => $plaintiffId,
        'commentId' => $commentId,
        'type' => 'comment'
    ];

    DB::insert('reports', $reportObj);
    $insertId = DB::insertId();
    $log->debug("Record report added id=" . $insertId);
    $response = $response->withStatus(201);
    $response->getBody()->write(json_encode($insertId));
    return $response;
});

function validateRegistration($user, $forPatch = false)
{
    if ($user === NULL) {
        return "Invalid JSON data provided";
    }

    $expectedFields = ['firstName', 'middleName', 'lastName', 'emailAddress', 'dateOfBirth', 'currentCity', 'password'];
    $registerFields = array_keys($user);

    // Check for missing fields
    if ($diff = array_diff($registerFields, $expectedFields)) {
        return "Missing fields in user: [" . implode(',', $diff) . "]";
    }

    if (!$forPatch) {
        if ($diff = array_diff($expectedFields, $registerFields)) {
            return "Missing fields in user: [" . implode(',', $diff) . "]";
        }
    }

    // Check for nulls
    $nullableFields = ['middleName', 'currentCity'];
    foreach ($user as $key => $value) {
        if (!in_array($key, $nullableFields)) {
            if (@is_null($value)) {
                return "$key must not be null";
            }
        }
    }

    if (isset($user['firstName'])) {
        $firstName = $user['firstName'];
        if (!preg_match('/^[a-zA-Z0-9 \,\'\.\-]{2,100}$/', $firstName)) {
            return "First name must be 2-100 characters long. Accepted characters are upper/lower case letters, digits, commas, periods and dashes";
        }
    }

    if (isset($user['middleName'])) {
        $middleName = $user['middleName'];
        if ($middleName != "" || $middleName != null) {
            if (!preg_match('/^[a-zA-Z0-9 \,\'\.\-]{2,100}$/', $middleName)) {
                return "Middle name must be 2-100 characters long. Accepted characters are upper/lower case letters, digits, commas, periods and dashes";
            }
        }
    }

    if (isset($user['lastName'])) {
        $lastName = $user['lastName'];
        if (!preg_match('/^[a-zA-Z0-9 \,\'\.\-]{2,100}$/', $lastName)) {
            return "Middle name must be 2-100 characters long. Accepted characters are upper/lower case letters, digits, commas, periods and dashes";
        }
    }

    if (isset($user['emailAddress'])) {
        $emailAddress = $user['emailAddress'];
        if (filter_var($emailAddress, FILTER_VALIDATE_EMAIL) === FALSE) {
            return "Email address must be formatted as john@example.com";
        }
    }

    if (isset($user['dateOfBirth'])) {
        $dateOfBirth = $user['dateOfBirth'];
        $dateOfBirth = str_replace('/', '-', $dateOfBirth);
        $dateOfBirth = date('Y-m-d', strtotime($dateOfBirth));

        $user['dateOfBirth'] = $dateOfBirth;

        if ($dateOfBirth < strtotime('1900-01-01') || $dateOfBirth > strtotime('2020-01-01')) {
            return "Date of birth must be between 1900-01-01 and 2020-01-01";
        }
    }

    if (isset($user['currentCity'])) {
        $currentCity = $user['currentCity'];
        if ($currentCity != "" || $currentCity != null) {
            if (!preg_match('/^[a-zA-Z0-9 \,\'\.\-]{2,100}$/', $currentCity)) {
                return "Current city must be 2-100 characters long. Accepted characters are upper/lower case letters, digits, commas, periods and dashes";
            }
        }
    }

    if (isset($user['password'])) {
        $password = $user['password'];
        if (!preg_match('/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/', $password)) {
            return "Password must be minimum eight characters, at least one letter, one number and one special character";
        }
    }
    return TRUE;
}
